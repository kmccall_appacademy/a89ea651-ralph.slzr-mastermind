class Code
  attr_reader :pegs
  PEGS = {
    'R' => :red,
    'G' => :green,
    'B' => :blue,
    'Y' => :yellow,
    'O' => :orange,
    'P' => :purple
  }

  def self.parse(code_input)
    pegs = code_input.split('').map do |letter|
      raise 'parse error' unless PEGS.has_key?(letter.upcase)
      PEGS[letter.upcase]
    end

    Code.new(pegs)
  end

    def self.random
      # pegs = []
      # 4.times { pegs << PEGS.values.sample }
      pegs = PEGS.values.sample(4)
      Code.new(pegs)
    end

    def initialize(pegs)
      @pegs = pegs
    end

    def [](i)
      pegs[i]
    end

  def exact_matches(other_input)
    exact_matches = 0
    pegs.each_index do |idx|
      exact_matches += 1 if pegs[idx] == other_input[idx]
    end
    exact_matches
  end

  def near_matches(other_input)
    hash1 = {}
    hash2 = {}
    4.times do |i|
      unless self[i] == other_input[i]
        if hash1.keys.include?(self[i])
          hash1[self[i]] += 1
        else
          hash1[self[i]] = 1
        end
        if hash2.keys.include?(other_input[i])
          hash2[other_input[i]] += 1
        else
          hash2[other_input[i]] = 1
        end
      end
    end
    near_matches = 0
    (hash1.keys.uniq & hash2.keys.uniq).each do |color|
      near_matches += [hash1[color], hash2[color]].min
    end
    near_matches
  end

  def ==(other_input)
    return false unless other_input.is_a?(Code)
    self.pegs == other_input.pegs
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "please make a guess"
    guess = Code.new(gets.chomp)
  end

  def display_matches(other_input)
    exact = @secret_code.exact_matches(other_input)
    near = @secret_code.near_matches(other_input)

    puts "#{exact} exact matches"
    puts "#{near} near matches"
  end
end
